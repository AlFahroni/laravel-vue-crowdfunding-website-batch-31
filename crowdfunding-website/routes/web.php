<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/{any?}', 'app')->where('any', '.*');

// Route::get('/{any?}', function(){
//     return 'masuk ke sini';
// })->where('any', '.*');


// Route::get('/', function () {
//     return view('app');
// });

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/route-1', function () {
//     return 'user sudah verifikasi';
// })->middleware('auth', 'emailVerified');

// Route::get('/route-2', function () {
//     return 'anda adalah admin';
// })->middleware(['auth', 'admin', 'emailVerified']);

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
